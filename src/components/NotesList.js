import ForgeUI, { Fragment } from '@forge/ui'
import Note from './Note'

const NotesList = ({notes}) => (
	<Fragment>
		{notes.map(note => <Note text={note.text} accountId={note.accountId}/>)}
	</Fragment>

)

export default NotesList