import ForgeUI, {Fragment, Text, Avatar} from '@forge/ui'

const Note = ({text, accountId}) => (
	<Fragment>
		<Avatar accountId={accountId}/>
		<Text>{text}</Text>
	</Fragment>
)

export default Note