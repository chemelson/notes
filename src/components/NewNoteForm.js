import ForgeUI, {ModalDialog, Form, TextArea} from '@forge/ui'

const NewNoteForm = ({show, submit}) => (
	<ModalDialog header='Create Note' onClose={() => show(false)}>
		<Form onSubmit={submit}>
			<TextArea name='text' label='' placeholder='Type your note...'/>
		</Form>
	</ModalDialog>
)

export default NewNoteForm
