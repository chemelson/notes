import ForgeUI, {Form, Fragment, ModalDialog, Select, Option} from '@forge/ui'

const DeleteNoteForm = ({show, submit, notes}) => {
	const options = notes.map(note => <Option label={note.text} value={note.text}/>)

	return (<Fragment>
		<ModalDialog header='Delete Note' onClose={() => show(false)}>
			<Form onSubmit={submit}>
				<Select label='' name='text'>
					{options}
				</Select>
			</Form>
		</ModalDialog>
	</Fragment>)
}

export default DeleteNoteForm
