import ForgeUI, {render, IssueActivity, useState, useProductContext, Fragment, Button, ButtonSet} from '@forge/ui'
import {useIssueProperty} from '@forge/ui-jira';
import NotesList from './components/NotesList'
import NewNoteForm from "./components/NewNoteForm";
import DeleteNoteForm from "./components/DeleteNoteForm";

const App = () => {
	const {accountId} = useProductContext()
	const [notes, setNotes] = useIssueProperty('issueNotes', [])
	const [showCreate, setShowCreate] = useState(false)
	const [showDelete, setShowDelete] = useState(false)

	const createNote = async formData => {
		const text = formData.text
		const updatedNotes = [...notes, {accountId, text}]
		setShowCreate(false)
		await setNotes(updatedNotes)
	}

	const deleteNote = async formData => {
		const text = formData.text
		const updatedNotes = notes.filter(note => note.text !== text)
		setShowDelete(false)
		await setNotes(updatedNotes)
	}

	return (
		<Fragment>
			<NotesList notes={notes}/>
			<ButtonSet>
				<Button text="Create" onClick={() => setShowCreate(true)}/>
				<Button text="Delete" onClick={() => setShowDelete(true)}/>
			</ButtonSet>
			{showCreate && <NewNoteForm show={setShowCreate} submit={createNote}/>}
			{showDelete && <DeleteNoteForm show={setShowDelete} submit={deleteNote} notes={notes}/>}
		</Fragment>
	)
}

export const run = render(
	<IssueActivity>
		<App/>
	</IssueActivity>
);